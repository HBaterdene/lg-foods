import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import mnMN from 'antd/lib/locale/mn_MN';
import { ConfigProvider } from 'antd';
import App from './pages/App';
import {initializeStore} from "./redux/store"
import reportWebVitals from './reportWebVitals';

const store = initializeStore();
ReactDOM.render(
  <Provider store={ store }>
    <ConfigProvider locale={mnMN}>
      <App />
    </ConfigProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
