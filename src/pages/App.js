import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "../styles/antd.less";
import "../styles/styles.scss";
import Home from "./home"
import Shop from "./shop/shop"
import Cart from "./shop/cart"
import Wishlist from "./shop/wishlist";
import Checkout from "./shop/checkout"
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/shop/checkout" component={Checkout}/>
        <Route path="/shop/wishlist" component={Wishlist}/>
        <Route path="/shop/cart" component={Cart}/>
        <Route path="/shop" component={Shop}/>
        <Route path="/" component={Home}/>
      </Switch>
    </Router> 
  );
}

export default App;
