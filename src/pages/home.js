import React from 'react';
import Header from '../components/header';
import HeroSlider from '../components/sections/HeroSlider';
import heroData from "../data/sections/hero-slider.json";
import categoriesOneData from "../data/sections/categories.json";
import Categories from "../components/sections/Categories";
import Benefits from '../components/other/Benefits';
import Container from '../components/other/container';
import Introduction from '../components/sections/Introduction'
import IntroductionData from "../data/sections/introduction.json"
import Layout from '../components/layout/Layout';
const Home = () => {
  return (
    <Layout>
      <HeroSlider data={heroData} threeCol={true}/>
      <Container>
        <Benefits
          threeCol
          style={{
            marginTop: -75 / 16 + "em",
            position: "relative",
            zIndex: 2,
          }}
        />
      </Container>
      <Categories data={categoriesOneData.one}/>
      <Introduction data={IntroductionData} />
    </Layout>
  )
}

export default Home;
