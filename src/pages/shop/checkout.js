/* eslint-disable no-mixed-operators */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Formik } from "formik";
import { Form, Input, FormItem, DatePicker } from "formik-antd";
import * as Yup from "yup";
import { Button, Checkbox, Row, Col, Select, Radio, Breadcrumb } from "antd";
import LocationSelector from "../../components/shared-components/LocationSelector";
import { useHistory } from "react-router-dom";
import { FaHome } from "react-icons/fa";
import { formatCurrency } from "../../common/utils";
import { calculateTotalPrice } from "../../common/shopUtils";
import Layout from "../../components/layout/Layout";
import Container from "../../components/other/container";
import ShopOrderStep from "../../components/shop/ShopOrderStep";
import PartnerOne from "../../components/sections/partners/PartnerOne";
import FetchDataHandle from "../../components/other/FetchDataHandle";

const date = new Date();
date.setDate(date.getDate() + 1);
const FormSchema = Yup.object().shape({
  name: Yup.string().required("Өөрийн нэрээ оруулна уу!"),
  // email: Yup.string().email("Та зөв е-шуудан оруулна уу").required("Е-шуудангаа оруулна уу!"),
  email: Yup.string().email(),
  phone1: Yup.string()
    .min(8, "Та хамгийн багадаа 8 оронтой тоо оруулана уу")
    .required("Утасны дугаараа оруулна уу!"),
  phone2: Yup.string()
    .min(8, "Та хамгийн багадаа 8 оронтой тоо оруулана уу")
    .required("Утасны дугаараа оруулна уу!"),
  detailed_location: Yup.string()
    .min(100, "Хамгийн багадаа зуун тэмдэгт оруулана уу")
    .required("Ta дэлгэрэнгүй хаягаа оруулана уу"),
  extra_info: Yup.string(),
  send_date: Yup.date(
    "Ta захиалгаа хүлээн авах өдрөө заавал сонгоно уу"
  ).required("Ta захиалгаа хүлээн авах өдрөө заавал сонгоно уу"),
  send_hour: Yup.string().required("Хүргэлт хүлээн авах цагаа оруулана уу"),
});
const {Option} = Select

function checkout() {
  const [paymentMethod, setPaymentMethod] = useState("cod");
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone1: "",
    phone2: "",
    detailed_location: "",
    extra_info: "",
    send_date: date,
    send_hour: "",
  });
  const history = useHistory();
  const cartState = useSelector((state) => state.cartReducer);
  const onChoosePaymentMethod = (e) => {
    setPaymentMethod(e.target.value);
  };
  const onSubmit = async (values) => {
    await new Promise((resolve) => setTimeout(resolve, 500));
    alert(JSON.stringify(values, null, 2));
  };

  return (
    <Formik
      initialValues={formData}
      validationSchema={FormSchema}
      onSubmit={onSubmit}
    >
      <Layout title="Checkout">
        <Container>
          <Breadcrumb separator=">">
            <Breadcrumb.Item>
              <FaHome />
              Home
            </Breadcrumb.Item>
            <Breadcrumb.Item>Shop</Breadcrumb.Item>
            <Breadcrumb.Item>Checkout</Breadcrumb.Item>
          </Breadcrumb>
          <ShopOrderStep current={2} />
          <FetchDataHandle
            emptyDescription="No product in cart"
            data={cartState}
            renderData={(data) => (
              <div className="checkout">
                <Row gutter={50}>
                  <Col xs={24} md={16}>
                    <div className="checkout-form">
                      <h3 className="checkout-title">Billing details</h3>
                      <Form
                        name="checkout"
                        layout="vertical"
                        id="checkout-form"
                      >
                        <Row gutter={15}>
                          <Col xs={24} sm={12}>
                            <FormItem label="Нэр" name="name">
                              <Input name="name" />
                            </FormItem>
                          </Col>
                          <Col xs={24} sm={12}>
                            <FormItem label="И-мэйл" name="email">
                              <Input name="title" />
                            </FormItem>
                          </Col>
                          <Col xs={24} sm={12}>
                            <FormItem label="Утас 1" name="phone1">
                              <Input name="phone1" />
                            </FormItem>
                          </Col>
                          <Col xs={24} sm={12}>
                            <FormItem label="Утас 2" name="phone2">
                              <Input name="phone2" />
                            </FormItem>
                          </Col>
                          <Col span={24}>
                            <LocationSelector name="address" />
                          </Col>
                          <Col span={24}>
                            <FormItem
                              name="detailed_location"
                              label="Дэлгэрэнгүй хаяг"
                            >
                              <Input
                                name="detailed_location"
                                placeholder="Та энд Хотхон, Байр/Гудамж, Хаалганы дугаараа дэлгэрэнгүй оруулна уу"
                              />
                            </FormItem>
                          </Col>
                          <Col span={24}>
                            <Row gutter={16}>
                              <Col sm={16} xs={24}>
                                <FormItem
                                  name="extra_info"
                                  label="Нэмэлт мэдээлэл"
                                >
                                  <Input.TextArea
                                    name="extra_info"
                                    placeholder="Хүргэлтийн үйлчилгээний үед анхаарах нэмэлт мэдээллээ үлдээнэ үү!"
                                  />
                                </FormItem>
                              </Col>
                              <Col sm={8} xs={24}>
                                <Row>
                                  <Col span={24}>
                                    <FormItem
                                      name="send_date"
                                      label="Хүргэлтээр авах өдөр"
                                      style={{marginBottom: 11}}
                                    >
                                      <DatePicker
                                        name="send_date"
                                        disabledDate={function (current) {
                                          const date = new Date();
                                          date.setDate(date.getDate() + 14);
                                          // Can not select days before today and today
                                          return (
                                            (current &&
                                              current.valueOf() < Date.now()) ||
                                            current.valueOf() > date
                                          );
                                        }}
                                      />
                                    </FormItem>
                                  </Col>
                                  <Col span={24}>
                                    <FormItem
                                      name="send_hour"
                                      label="Хүргэлтээр авах цаг"
                                    >
                                      <Select name="send_hour">
                                        <Option value="day">12:00-16:00 (Өдөр)</Option>
                                        <Option value="night">16:00-21:00 (Орой)</Option>
                                      </Select>
                                    </FormItem>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Form>
                    </div>
                  </Col>
                  <Col xs={24} md={8}>
                    <div className="checkout-total">
                      <h3 className="checkout-title">Your order</h3>
                      <table className="checkout-total__table">
                        <tbody>
                          {data.map((item, index) => (
                            <tr key={index}>
                              <td>
                                {item.name} x {item.cartQuantity}
                              </td>
                              <td className="-bold ">
                                {formatCurrency(item.price * item.cartQuantity)}
                              </td>
                            </tr>
                          ))}
                          <tr>
                            <th>SUBTOTAL</th>
                            <td className="-bold -color">
                              {formatCurrency(calculateTotalPrice(data))}
                            </td>
                          </tr>
                          <tr>
                            <th>SHIPPING</th>
                            <td>
                              <p>Free shipping</p>
                              <p>Calculate shipping</p>
                            </td>
                          </tr>
                          <tr>
                            <th>Total</th>
                            <td
                              style={{ fontSize: 20 / 16 + "em" }}
                              className="-bold -color"
                            >
                              {formatCurrency(calculateTotalPrice(data))}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <div className="checkout-total__footer">
                        <Radio.Group
                          onChange={onChoosePaymentMethod}
                          value={paymentMethod}
                        >
                          <Radio style={{ display: "block" }} value="cod">
                            Cash on delivery
                          </Radio>
                          <Radio style={{ display: "block" }} value="paypal">
                            Paypal
                          </Radio>
                        </Radio.Group>
                      </div>
                      <Button
                        className="checkout-sumbit"
                        type="primary"
                        shape="round"
                        form="checkout-form"
                        key="submit"
                        htmlType="submit"
                      >
                        <a>Place order</a>
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            )}
          />
          <PartnerOne />
        </Container>
      </Layout>
    </Formik>
  );
}

export default React.memo(checkout);
