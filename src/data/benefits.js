/* eslint-disable import/no-anonymous-default-export */
import {RiTruckLine, BsClock, ImCreditCard} from "react-icons/all"

export default [
  {
    iconSrc: <RiTruckLine/>,
    name: "Үнэгүй хүргэлт",
    description: "20000₮ дээш бүх хүргэлтэнд"
  },
  {
    iconSrc: <BsClock/>,
    name: "Яг цагтаа",
    description: "Таны захиалгыг хурдан шуурхай хүргэнэ"
  },
  {
    iconSrc: <ImCreditCard/>,
    name: "Олон төрлийн төлбөр төлөх боломж",
    description: "Бүх банк, санхүүгийн төлбөрийн систем"
  },
]