import React from "react";
import {Link} from "react-router-dom";

import SocialIcons from "../../other/socialIcons";

export default function FooterInfomation() {
  return (
    <div className="footer-info">
      <Link className="footer-info__logo" to={"/"}>
          <img
            src={"/assets/images/logo.png"}
            alt="LG Food Logo"
          />
      </Link>
      <ul>
        <li>Хаяг: Улаанбаатар, Хануул дүүрэг</li>
        <li>Утас: +976 7741 1315</li>
        <li>Email: Lgfoodmongolia@gmail.com</li>
      </ul>
      <SocialIcons type="primary" shape="circle" className="-btn-light" />
    </div>
  );
}
