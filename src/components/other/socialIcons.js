import React from "react";
import classNames from "classnames";
import { Button } from "antd";
import {FaFacebookF, FaTwitter, FaInvision, FaPinterestP} from "react-icons/fa"
export default function SocialIcons({ className, type = "link", shape }) {
  return (
    <ul className={`social-icons ${classNames(className)}`}>
      <li>
        <Button type={type} shape={shape} href="#">
          <FaFacebookF/>
        </Button>
      </li>
      <li>
        <Button type={type} shape={shape} href="#">
          <FaTwitter/>
        </Button>
      </li>
      <li>
        <Button type={type} shape={shape} href="#">
          <FaInvision/>
        </Button>
      </li>
      <li>
        <Button type={type} shape={shape} href="#">
          <FaPinterestP/>
        </Button>
      </li>
    </ul>
  );
}