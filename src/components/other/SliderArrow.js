import {FaAngleLeft, FaAngleRight} from "react-icons/fa"
import { Link } from 'react-router-dom';
export const PrevArrow = ({ currentSlide, slideCount, ...arrowProps }) => (
  <Link {...arrowProps} >
    <FaAngleLeft classname="slick-arrow--left"/>
  </Link>
);

export const NextArrow = ({ currentSlide, slideCount, ...arrowProps }) => (
  <Link {...arrowProps}>
    <FaAngleRight classname="slick-arrow--right"/>
  </Link>
);
