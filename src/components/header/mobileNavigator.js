import React from "react";
import { Menu } from "antd";
import { NavLink } from "react-router-dom";
import SocialIcons from "../other/socialIcons";

function MobileNavigator() {
  // const handleClick = (e) => {
  //   console.log("click ", e);
  //   this.setState({ current: e.key });
  // };
  return (
    <div className="menu-mobile">
      <Menu
        className="menu-mobile-navigator"
        // onClick={handleClick}
        mode="inline"
      >
        <Menu.Item><NavLink to="/">Эхлэл</NavLink></Menu.Item>
        <Menu.Item><NavLink to="/shop">Дэлгүүр</NavLink></Menu.Item>
        <Menu.Item><NavLink to="/location">Байршилууд</NavLink></Menu.Item>
        <Menu.Item><NavLink to="/about">Бидний тухай</NavLink></Menu.Item>
      </Menu>
      <div className="menu-mobile-functions">
         <NavLink to="/login" className="menu-mobile-functions__login">Login/Register</NavLink>
        <SocialIcons />
      </div>
    </div>
  );
}

export default React.memo(MobileNavigator);