import React from 'react'
import TopNav from "./topnav";
import Menu from "./menu";
const Header = () => {
  return (
    <div className="header">
      <TopNav containerFluid={false}/>
      <Menu containerFluid={true}/>
    </div>
  )
}

export default Header;
