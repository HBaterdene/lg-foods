import React from "react";
import { Collapse } from "antd";
import { DownOutlined } from "@ant-design/icons";
import {Link} from "react-router-dom";
import {FaBars} from "react-icons/fa"

let categories = [
  { name: "Fresh Meat", to: "/shop" },
  { name: "Vegetables", to: "/shop" },
  { name: "Fruit & Nut Gifts", to: "/shop" },
  { name: "Fresh Berries", to: "/shop" },
  { name: "Ocean Foods", to: "/shop" },
  { name: "Butter & Eggs", to: "/shop" },
  { name: "Fastfood", to: "/shop" },
  { name: "Fresh Onion", to: "/shop" },
  { name: "Papayaya & Crisps", to: "/shop" },
  { name: "Oatmeal", to: "/shop" },
  { name: "Fresh Bananas", to: "/shop" },
];

function CategoryColappse({ active }) {
  const { Panel } = Collapse;
  return (
    <div className="category-collapse">
      <Collapse
        bordered={false}
        defaultActiveKey={active ? "1" : null}
        expandIcon={({ isActive }) => (
          <DownOutlined rotate={isActive ? -180 : 0} />
        )}
        expandIconPosition="right"
      >
        <Panel
          header="Бүх дэлгүүрүүд"
          key="1"
          extra={<FaBars />}
        >
          <ul>
            {categories.map((item, index) => (
              <li key={index}>
                <Link to={item.to}>{item.name}</Link>
              </li>
            ))}
          </ul>
        </Panel>
      </Collapse>
    </div>
  );
}

export default React.memo(CategoryColappse);
