import React, { useState } from "react";
import { Drawer } from "antd";
import {FaBars} from "react-icons/fa";
import MobileNavigator from "./mobileNavigator";
import {Link} from "react-router-dom"
export default function MobileMenuOpener() {
  const [visible, setVisible] = useState(false);
  const onShowDrawer = () => {
    setVisible(true);
  };
  const onCloseDrawer = () => {
    setVisible(false);
  };
  return (
    <>
      <div onClick={onShowDrawer} className="menu-mobile-opener">
        <FaBars/>
      </div>
      <Drawer
        title="Close"
        closable={true}
        onClose={onCloseDrawer}
        visible={visible}
        placement="left"
        width={320}
      >
        <MobileNavigator />
      </Drawer>
    </>
  );
}