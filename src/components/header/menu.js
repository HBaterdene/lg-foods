/* eslint-disable no-lone-blocks */
import React from "react";
import { NavLink } from "react-router-dom";
import Container from "../other/container";
import FunctionItems from "./functionItems";
import MobileMenuOpener from "./mobileMenuOpener";
import LogoPNG from "../../assets/images/logo.png";
import SearchForm from "./SearchForm";
import { FiSearch } from "react-icons/fi";
export default function Menu({ containerFluid }) {
  return (
    <div className="menu -style-two">
      <Container fluid={containerFluid}>
        <div className="menu-wrapper">
          <MobileMenuOpener />
          <NavLink className="menu-logo" to="/">
            <img src={LogoPNG} alt="" />
          </NavLink>
          <SearchForm enterButton={<FiSearch />} />
          <ul className="navigation">
            <li className="navigation-item -toggleable">
              <NavLink to="/" className="navigation-item__title">
                Эхлэл
              </NavLink>
            </li>
            <li className="navigation-item -toggleable">
              <NavLink to="/shop" className="navigation-item__title">
                Дэлгүүр
              </NavLink>
            </li>
            <li className="navigation-item -toggleable">
              <NavLink to="/location" className="navigation-item__title">
                Байршилууд
              </NavLink>
            </li>
            <li className="navigation-item -toggleable">
              <NavLink to="/about" className="navigation-item__title">
                Бидний тухай
              </NavLink>
            </li>
          </ul>
          {/* <Navigator /> */}
          <FunctionItems />
        </div>
      </Container>
    </div>
  );
}
{
  /* <div className="menu -style-one">
<Container>
  <div className="menu-wrapper">
      <MobileMenuOpener />
      <NavLink className="menu-logo" to="/">
        <img src={LogoPNG} alt=""/>
      </NavLink>
      <ul className="navigation">
        <li className="navigation-item -toggleable">
          <NavLink to="/" className="navigation-item__title">Эхлэл</NavLink>
        </li>
        <li className="navigation-item -toggleable">
          <NavLink to="/shop" className="navigation-item__title">Дэлгүүр</NavLink>
        </li>
        <li className="navigation-item -toggleable">
          <NavLink to="/location" className="navigation-item__title">Байршилууд</NavLink>
        </li>
        <li className="navigation-item -toggleable">
          <NavLink to="/about" className="navigation-item__title">Бидний тухай</NavLink>
        </li>
      </ul>
    <FunctionItems />
  </div>
</Container>
</div> */
}
