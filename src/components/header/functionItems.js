import {Link} from "react-router-dom";
import React from "react";
import {AiOutlineHeart, IoBagOutline} from "react-icons/all"
import {useSelector} from "react-redux"
import {
  calculateTotalPrice
} from "../../common/shopUtils";
import { formatCurrency } from "../../common/utils";
function FunctionItems({ hideTotal, hideWishlist }) {
  const cartState = useSelector(state => state.cartReducer)
  return (
    <div className="function-items">
      {!hideWishlist && (
        <Link to="/shop/wishlist" className="function-items-item">
            <AiOutlineHeart/>
        </Link>
      )}
      <Link to="/shop/cart" className="function-items-item">
          <IoBagOutline/>
          <span> {formatCurrency(calculateTotalPrice(cartState.data))}</span>
      </Link>
    </div>
  );
}

export default React.memo(FunctionItems);
