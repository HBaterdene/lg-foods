import React from "react";
import Container from "../other/container";
import SocialIcons from "../other/socialIcons";
import { Select } from "antd";
import {FaEnvelope, FaPhoneAlt, FaUser} from "react-icons/fa";
import { Link } from "react-router-dom";

// const flagData = [
//   { name: "english", image: "/assets/images/header/flag-usa.png" },
//   { name: "japanese", image: "/assets/images/header/flag-jp.png" },
//   { name: "vietnamese", image: "/assets/images/header/flag-vn.png" },
// ];

export default function TopNav({ containerFluid }) {
  // const { Option } = Select;
  return (
    <div className="top-nav">
      <Container fluid={containerFluid}>
        <div className="top-nav-wrapper">
          <div className="top-nav-left">
            <ul>
              <li>
                <FaEnvelope/>
                info.deercreative@gmail.com
              </li>
              <li>
                <FaPhoneAlt/>
                +65 11.188.888
              </li>
            </ul>
          </div>
          <div className="top-nav-right">
            <div className="top-nav-right__item">
              <SocialIcons />
            </div>
            {/* <div className="top-nav-right__item">
              <Select defaultValue="english" width={125} bordered={false}>
                {flagData.map((item, index) => (
                  <Option key={index} value={item.name}>
                    <img
                      style={{
                        height: 13 / 16 + "em",
                        width: 20 / 16 + "em",
                        objectFit: "contain",
                        marginTop: -3 / 16 + "em",
                        marginRight: 5 / 16 + "em",
                      }}
                      src={process.env.PUBLIC_URL + item.image}
                      alt=""
                    />
                    {item.name}
                  </Option>
                ))}
              </Select>
            </div> */}
            <div className="top-nav-right__item">
              <Link to="">
                <Link>
                  <FaUser />
                  Login
                </Link>
              </Link>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
