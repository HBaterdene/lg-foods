import React from "react";

import Container from "../other/container";
import CategoryColappse from "./CategoryColappse";

import { Row, Col } from "antd";
import SearchForm from "./SearchForm";

function FunctionMenu({ activeHeaderCollapse }) {
  return (
    <div className="header-function-menu-one">
      <Container>
        <div className="function-menu-wrapper">
          <Row gutter={30}>
            {/* <Col xs={{ span: 24, order: 2 }} md={{ span: 8, order: 1 }} lg={6}>
              <CategoryColappse active={activeHeaderCollapse} />
            </Col> */}
            <Col
              xs={{ span: 24, order: 1 }}
              md={{ span: 24, order: 2 }}
              lg={24}
            >
              <SearchForm enterButton={<i className="icon_search" />}/>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}
export default React.memo(FunctionMenu);
