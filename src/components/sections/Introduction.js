import React from "react";
import { Button } from "antd";
import {Link} from "react-router-dom";

function Introduction({ data }) {
  return (
    <div className="introduction">
      {data.map((item, index) => (
        <div
          key={index}
          className="introduction-item"
          style={{ backgroundImage: `url('${item.background}')` }}
        >
          <img src={item.image} alt="Introduction" />
          <Button type="primary" shape="round">
            <Link to="/shop">
             Одоо авах
            </Link>
          </Button>
        </div>
      ))}
    </div>
  );
}

export default React.memo(Introduction);
