import React from "react";
import Container from "../other/container";
import { Col, Row, Empty } from "antd";
import { Link } from "react-router-dom";

function Categories({ data }) {
  return (
    <div className="categories">
      <Container>
        <Row gutter={[{ sm: 0, md: 15 }]}>
          {data && data.length > 0 ? (
            data.map((item, index) => (
              <Col key={index} xs={24} sm={12} md={6}>
                <Link to="/shop" className="categories-item">
                    <div className="categories-item__image up-down-anim-hover">
                      <span>{item.image.background}</span>
                      <img
                        src={item.image.main}
                        alt="Category item"
                      />
                    </div>
                    <h2>{item.title}</h2>
                    <p>{item.quantity} Бүтээгдэхүүн</p>
                </Link>
              </Col>
            ))
          ) : (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          )}
        </Row>
      </Container>
    </div>
  );
}

export default React.memo(Categories);
