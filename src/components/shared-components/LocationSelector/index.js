/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from "react";
import { Field } from "formik";
import { Select as AntSelect, Button, Form as AntForm, Col, Row } from "antd";

const duuregData = ["Баянгол дүүрэг", "Баянзүрх дүүрэг", "Сонгинохайрхан дүүрэг", "Сүхбаатар дүүрэг", "Хан-Уул дүүрэг", "Чингэлтэй дүүрэг"];
const horooData = {
  "Баянгол дүүрэг": 23,
  "Баянзүрх дүүрэг": 28,
  "Сонгинохайрхан дүүрэг": 32,
  "Сүхбаатар дүүрэг": 20,
  "Хан-Уул дүүрэг": 23,
  "Чингэлтэй дүүрэг": 19
};
const horooName = (num) => {
  console.log("hey---------->", num)
  const arr = [];
  for (let i = 1; i < num + 1; i++) {
    console.log("hey---------->", i)
    arr.push(`${i}-р хороо`)    
  }
  return arr
}
export default ({ name, limit, onChange }) => {
  return (
    <Field name={name}>
      {
        fieldProps => {

          const {
            field: { value },
            form: { setFieldValue, setFieldTouched, setFieldError, values, setValues }
          } = fieldProps;
         
          const [horoos, setHoroos] = React.useState(horooName(horooData[duuregData[0]]));
          const [horoo, setHoroo] = React.useState(null);
          console.log("horoos", horoos);
          const handleDuuregChange = district => {
            setHoroos(horooName(horooData[district]));
            setFieldValue({duureg: district, horoo: null })
            // setHoroo(horooName(horooData[district])[0]);
          };
          const onHorooChange = street => {
            setHoroo(street);
            setFieldValue({ ...value, horoo: street })
          };
          return (
      <Row gutter={15}>
        <Col xs={24} sm={8}>
       <AntForm.Item label="Аймаг/Хот">
        <AntSelect value="Улаанбаатар хот" disabled>
           <AntSelect.Option value="Улаанбаатар хот">
              Улаанбаатар хот
           </AntSelect.Option>
        </AntSelect> 
       </AntForm.Item>
       </Col>
       <Col xs={24} sm={8}>
       <AntForm.Item label="Сум/Дүүрэг">
       <AntSelect onChange={handleDuuregChange}>
        {duuregData.map(duureg => (
          <AntSelect.Option key={duureg}>{duureg}</AntSelect.Option>
        ))}
      </AntSelect> 
       </AntForm.Item>
       </Col>
       <Col xs={24} sm={8}>
       <AntForm.Item label="Баг/Хороо">
       <AntSelect onChange={onHorooChange} value={horoo}>
        {horoos.map(horoo => (
          <AntSelect.Option key={horoo} >{horoo}</AntSelect.Option>
        ))}
      </AntSelect> 
       </AntForm.Item>
       </Col>
     </Row>
      )
    }
    }
    </Field>
  );
};