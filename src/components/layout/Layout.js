import React from "react";
import { BackTop } from "antd";

import Header from "../header";
import Footer from "../footer/Footer";
import withHeaderScroll from "../../common/withHeaderScroll";

const ScrolledHeader = withHeaderScroll(Header);

function Layout({ children, headerClass, footerClass }) {
  return (
    <>
      <ScrolledHeader className={headerClass} />
      {children}
      <Footer className={footerClass} />
      <BackTop />
    </>
  );
}

export default React.memo(Layout);
